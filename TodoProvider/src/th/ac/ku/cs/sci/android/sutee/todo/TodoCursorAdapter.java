package th.ac.ku.cs.sci.android.sutee.todo;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

public class TodoCursorAdapter extends CursorAdapter {

	public TodoCursorAdapter(Context context, Cursor c) {
		super(context, c, 0);
	}

	public static final class ViewHolder {
		CheckedTextView checkedTextView;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder viewHolder = (ViewHolder) view.getTag();		
		Task task = new Task();
		task.fromCursor(cursor, context);
		viewHolder.checkedTextView.setText(task.getName());
		viewHolder.checkedTextView.setChecked(task.isDone());
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View view = LayoutInflater.from(mContext).inflate(R.layout.row, parent,
				false);
		ViewHolder viewHolder = new ViewHolder();
		viewHolder.checkedTextView = (CheckedTextView) view
				.findViewById(R.id.checkedTextView);
		view.setTag(viewHolder);
		return view;
	}

}
