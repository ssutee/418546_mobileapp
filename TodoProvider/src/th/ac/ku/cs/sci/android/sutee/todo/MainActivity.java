package th.ac.ku.cs.sci.android.sutee.todo;

import java.util.Date;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends FragmentActivity implements
		LoaderCallbacks<Cursor> {

	TodoCursorAdapter mAdapter;
	Dao<Task> taskDao;
	ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		listView = (ListView) findViewById(R.id.listView);
		txtRemainingTasks = (TextView) findViewById(R.id.txtRemainingTasks);

		taskDao = new Dao<Task>(Task.class, this, TodoProvider.TASK_CONTENT_URI);
		mAdapter = new TodoCursorAdapter(this, null);
		listView.setAdapter(mAdapter);

		getSupportLoaderManager().initLoader(0, null, this);

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int position, long id) {
				Task task = taskDao.get(position);
				task.setDone(!task.isDone());
				taskDao.update(task);
			}
		});
	}

	private TextView txtRemainingTasks;

	private void updateRemainingTasksText() {
		txtRemainingTasks.setText(String.format("Remaining Tasks (%d/%d)",
				taskDao.size() - TaskHelper.getCountDoneTasks(taskDao),
				taskDao.size()));
	}

	public void addTask(View view) {
		EditText edtTaskName = (EditText) findViewById(R.id.edtTaskName);
		Task task = new Task();
		task.setName(edtTaskName.getText().toString());
		task.setDone(false);
		task.setCreatedAt(new Date());
		taskDao.insert(task);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle data) {
		return new CursorLoader(this, TodoProvider.TASK_CONTENT_URI, null,
				null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		mAdapter.swapCursor(cursor);
		 updateRemainingTasksText();
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}

}
