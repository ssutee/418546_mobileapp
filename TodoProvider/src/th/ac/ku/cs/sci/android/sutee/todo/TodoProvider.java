package th.ac.ku.cs.sci.android.sutee.todo;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.provider.BaseColumns;

public class TodoProvider extends ContentProvider {
    private TodoOpenHelper dbHelper;
    private SQLiteDatabase database;
    public static final String AUTHORITY = "th.ac.ku.cs.sci.android.sutee.todo.contentprovider";
    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private static final int TASKS = 1001;
    private static final int TASK_ID = 1002;
    public static final String TASK_PATH = "tasks";
    public static final Uri TASK_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TASK_PATH);
    public static final String TASK_CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/tasks";
    public static final String TASK_CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/task";
    static {
        sURIMatcher.addURI(AUTHORITY, TASK_PATH, TASKS);
        sURIMatcher.addURI(AUTHORITY, TASK_PATH + "/#", TASK_ID);
    }



    @Override
    public boolean onCreate() {
        dbHelper = new TodoOpenHelper(getContext());
        database = dbHelper.getWritableDatabase();
        return true;
    }

    @Override
    public String getType(Uri uri) {
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
        case TASKS:
            return TASK_CONTENT_TYPE;
        case TASK_ID:
            return TASK_CONTENT_ITEM_TYPE;
        }
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        long id = 0;
        switch (uriType) {
        case TASKS:
            id = database.insert(TaskTable.TABLE_NAME, null, values);
            getContext().getContentResolver().notifyChange(uri, null);
            return Uri.parse("content://" + AUTHORITY + "/" + TASK_PATH + "/" + id);
        default:
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        int uriType = sURIMatcher.match(uri);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        switch (uriType) {
        case TASKS:
            queryBuilder.setTables(TaskTable.TABLE_NAME);
            break;
        case TASK_ID:
            queryBuilder.setTables(TaskTable.TABLE_NAME);
            queryBuilder.appendWhere(BaseColumns._ID + "=" + uri.getLastPathSegment());
            break;
        default:
            throw new IllegalArgumentException("Unknown URI: " + uri);        
        }
        Cursor cursor = queryBuilder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        int rowsDeleted = 0;
        switch (uriType) {
        case TASKS:
            rowsDeleted = database.delete(TaskTable.TABLE_NAME, selection, selectionArgs);
            break;
        case TASK_ID:
            String taskId = uri.getLastPathSegment();
            if (TextUtils.isEmpty(selection)) {
                rowsDeleted = database.delete(TaskTable.TABLE_NAME, BaseColumns._ID + "=" + taskId, null);
            } else {
                rowsDeleted = database.delete(TaskTable.TABLE_NAME, BaseColumns._ID + "=" + taskId + " AND " + selection, selectionArgs);
            }
            break;
        default:
            throw new IllegalArgumentException("Unknown URI: " + uri);      
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        int rowsUpdated = 0;
        switch (uriType) {
        case TASKS:
            rowsUpdated = database.update(TaskTable.TABLE_NAME, values, selection, selectionArgs);
            break;
        case TASK_ID:
            String taskId = uri.getLastPathSegment();
            if (TextUtils.isEmpty(selection)) {
                rowsUpdated = database.update(TaskTable.TABLE_NAME, values, BaseColumns._ID + "=" + taskId, null);
            } else {
                rowsUpdated = database.update(TaskTable.TABLE_NAME, values, BaseColumns._ID + "=" + taskId + " AND " + selection, selectionArgs);
            }
            break;
        default:
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }


}