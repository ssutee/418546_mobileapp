package th.ac.ku.cs.sci.android.sutee.todo;

public class TaskHelper {
	public static int getCountDoneTasks(Dao<Task> taskDao) {
		return taskDao.get(TaskTable.TaskColumns.DONE + " = " + 1, null).size();
	}
}
