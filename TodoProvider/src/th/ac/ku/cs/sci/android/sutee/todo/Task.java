package th.ac.ku.cs.sci.android.sutee.todo;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;

public class Task extends ModelBase {
    @SuppressWarnings("unused")
	private Context context;
    private int id;
    private String name;
    private boolean done;
    private Date createdAt;


    public Task() {
        super();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void fromCursor(Cursor cursor, Context context) {
        this.id = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));
        this.name = cursor.getString(cursor.getColumnIndex(TaskTable.TaskColumns.NAME));
        this.done = cursor.getInt(cursor.getColumnIndex(TaskTable.TaskColumns.DONE)) == 1;
        this.createdAt = new Date(cursor.getLong(cursor.getColumnIndex(TaskTable.TaskColumns.CREATED_AT)));
        this.context = context;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(TaskTable.TaskColumns.NAME, this.name);
        values.put(TaskTable.TaskColumns.DONE, this.done);
        values.put(TaskTable.TaskColumns.CREATED_AT, this.createdAt.getTime());
        return values;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setId(int id) {
		this.id = id;
	}
}