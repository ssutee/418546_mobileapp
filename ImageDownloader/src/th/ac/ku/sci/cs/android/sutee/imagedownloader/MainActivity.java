package th.ac.ku.sci.cs.android.sutee.imagedownloader;

import th.ac.ku.sci.cs.android.sutee.imagedownloader.RestClient.OnRequestFinishListener;
import th.ac.ku.sci.cs.android.sutee.imagedownloader.RestClient.RequestMethod;
import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends ListActivity implements
		OnRequestFinishListener {

	private static final String TAG = "MainActivity";
	private MovieAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// RestClient restClient = new RestClient("http://www.ku.ac.th");
		// restClient.setOnRequestFinishListener(this);
		// restClient.execute(RequestMethod.GET);

		mAdapter = new MovieAdapter(this);
		setListAdapter(mAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public void onRequestFinish(RequestMethod method, int responseCode,
			String message, String response) {
		Log.d(TAG, response);
	}

	@Override
	public void onRequestFinishWithError(Exception e) {

	}
}
