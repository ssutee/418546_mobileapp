package th.ac.ku.sci.cs.android.sutee.imagedownloader;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

public class MovieAdapter extends BaseAdapter implements ListAdapter {

	private static final class ViewHolder {
		TextView txtMovieTitle;
		ImageView imgMovieThumb;
	}

	private Context mContext;
	private String[] movieTitles, movieThumbs;
	private ThreadPoolExecutor executor;
	private Handler handler = new Handler();

	public MovieAdapter(Context context) {
		mContext = context;
		movieTitles = context.getResources().getStringArray(R.array.movies);
		movieThumbs = context.getResources().getStringArray(
				R.array.movie_thumbs);
		executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);
	}

	@Override
	public int getCount() {
		return movieTitles.length;
	}

	@Override
	public Object getItem(int position) {
		return new Pair<String, String>(movieTitles[position],
				movieThumbs[position]);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.movie_row, parent, false);
			viewHolder.imgMovieThumb = (ImageView) convertView
					.findViewById(R.id.imgMovieThumb);
			viewHolder.txtMovieTitle = (TextView) convertView
					.findViewById(R.id.txtMovieTitle);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		@SuppressWarnings("unchecked")
		Pair<String, String> pair = (Pair<String, String>) getItem(position);
		viewHolder.txtMovieTitle.setText(pair.first);
		viewHolder.imgMovieThumb.setTag(position);
		downloadImage(position, pair.second, viewHolder.imgMovieThumb);
		return convertView;
	}

	private void downloadImage(final int position, final String imageUrl,
			final ImageView imageView) {

		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					URL url = new URL(imageUrl);
					final Bitmap image = BitmapFactory.decodeStream(url
							.openStream());
					handler.post(new Runnable() {
						@Override
						public void run() {
							if (position == (Integer) imageView.getTag()) {
								imageView.setImageBitmap(image);
							}
						}
					});
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}
}
