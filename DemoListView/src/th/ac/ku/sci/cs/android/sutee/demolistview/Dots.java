package th.ac.ku.sci.cs.android.sutee.demolistview;

import java.util.ArrayList;
import java.util.List;

public class Dots {
	private List<Dot> dots;
	private int autoId = 0;

	public interface OnDotsChangeListener {
		void onDotsChange(Dots dots);
	}

	private OnDotsChangeListener onDotsChangeListener;

	public void setOnDotsChangeListener(
			OnDotsChangeListener onDotsChangeListener) {
		this.onDotsChangeListener = onDotsChangeListener;
	}

	public Dots() {
		this.dots = new ArrayList<Dot>();
	}

	public void addDot(Dot dot) {
		autoId += 1;
		dot.setId(autoId);
		this.dots.add(dot);
		if (this.onDotsChangeListener != null) {
			this.onDotsChangeListener.onDotsChange(this);
		}
	}

	public int size() {
		return this.dots.size();
	}

	public Dot getDot(int position) {
		return this.dots.get(position);
	}
}
