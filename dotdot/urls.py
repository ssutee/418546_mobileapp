from django.conf.urls.defaults import patterns, include, url
from djangorestframework.views import ListOrCreateModelView, InstanceModelView
from dotdot.dots.resources import DotResource

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dotdot.dots.views.main_page', name='home'),
    # url(r'^dotdot/', include('dotdot.foo.urls')),
    url(r'^dots/$', ListOrCreateModelView.as_view(resource=DotResource), name='dots'),
    url(r'^dots/(?P<id>\d+)/$', InstanceModelView.as_view(resource=DotResource), name='dots'),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
