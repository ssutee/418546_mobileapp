package th.ac.ku.sci.cs.android.dotpush;

public class GCMUtilities {
	/**
	 * Base URL of the Demo Server (such as http://my_host:8080/gcm-demo)
	 */
	static final String SERVER_URL = "http://10.0.2.2:8000/devices/";

	/**
	 * Google API project id registered to use GCM.
	 */
	static final String SENDER_ID = "";
}
