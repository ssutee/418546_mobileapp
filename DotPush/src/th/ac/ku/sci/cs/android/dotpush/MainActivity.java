package th.ac.ku.sci.cs.android.dotpush;

import th.ac.ku.sci.cs.android.dotpush.model.DotProvider;
import th.ac.ku.sci.cs.android.dotpush.service.DotRestService;
import th.ac.ku.sci.cs.android.dotpush.service.IDotRestService;
import th.ac.ku.sci.cs.android.sutee.dotpush.R;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

public class MainActivity extends FragmentActivity implements
		LoaderCallbacks<Cursor> {

	@SuppressWarnings("unused")
	private static final String TAG = "MainActivity";

	private IDotRestService dotRestService;
	private ServiceConnection connection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			dotRestService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			dotRestService = IDotRestService.Stub.asInterface(service);
			try {
				dotRestService.fetchDots();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	};

	private Button btnRefresh;
	private ProgressBar spinner;
	private DotCursorAdapter mAdapter;
	private ListView mListView;

	@Override
	protected void onStart() {
		super.onStart();
		bindService(new Intent(this, DotRestService.class), connection,
				Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		unbindService(connection);
		super.onStop();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		btnRefresh = (Button) findViewById(R.id.btnRefresh);
		spinner = (ProgressBar) findViewById(R.id.spinner);

		mListView = (ListView) findViewById(R.id.listView);
		mAdapter = new DotCursorAdapter(this, null);
		mListView.setAdapter(mAdapter);

		getSupportLoaderManager().initLoader(0, null, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle data) {
		return new CursorLoader(this, DotProvider.DOT_CONTENT_URI, null, null,
				null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		btnRefresh.setVisibility(View.VISIBLE);
		spinner.setVisibility(View.INVISIBLE);
		mAdapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}

	public void refresh(View view) {
		btnRefresh.setVisibility(View.INVISIBLE);
		spinner.setVisibility(View.VISIBLE);
		try {
			dotRestService.fetchDots();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

}
