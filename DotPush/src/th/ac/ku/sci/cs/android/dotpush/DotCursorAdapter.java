package th.ac.ku.sci.cs.android.dotpush;

import th.ac.ku.sci.cs.android.dotpush.model.Dot;
import th.ac.ku.sci.cs.android.dotpush.model.Dot.State;
import th.ac.ku.sci.cs.android.sutee.dotpush.R;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DotCursorAdapter extends CursorAdapter {

	private static final class ViewHolder {
		View row;
		TextView txtCoordX;
		TextView txtCoordY;
	}

	@SuppressWarnings("unused")
	private static final String TAG = "DotCursorAdapter";

	public DotCursorAdapter(Context context, Cursor cursor) {
		super(context, cursor, 0);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder viewHolder = (ViewHolder) view.getTag();
		Dot dot = Dot.newInstance(cursor, context);
		viewHolder.txtCoordX.setText(String.valueOf(dot.getX()));
		viewHolder.txtCoordY.setText(String.valueOf(dot.getY()));

		if (dot.getState() == State.DELETE) {
			viewHolder.txtCoordX.setTextColor(Color.GRAY);
			viewHolder.txtCoordY.setTextColor(Color.GRAY);
			viewHolder.row.setEnabled(false);
		} else {
			viewHolder.txtCoordX.setTextColor(Color.BLACK);
			viewHolder.txtCoordY.setTextColor(Color.BLACK);
			viewHolder.row.setEnabled(true);
		}
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View view = LayoutInflater.from(context).inflate(R.layout.dot_row,
				parent, false);
		ViewHolder viewHolder = new ViewHolder();
		viewHolder.row = view;
		viewHolder.txtCoordX = (TextView) view.findViewById(R.id.txtCoordX);
		viewHolder.txtCoordY = (TextView) view.findViewById(R.id.txtCoordY);
		view.setTag(viewHolder);
		return view;
	}

}
