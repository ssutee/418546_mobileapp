package th.ac.ku.sci.cs.android.dotpush.service;

import java.text.SimpleDateFormat;

public class DotRestUtilities {
	public static final String HOST = "http://10.0.2.2:8000";
	public static final String DOTS_URL = HOST + "/dots";
	public static final String UPDATED_AT_KEY = "updated_at";
	public static final String X_KEY = "x";
	public static final String Y_KEY = "y";
	public static final String RID_KEY = "pk";
	public static final String GID_KEY = "gid";
	public static final String RADIUS_KEY = "radius";
	public static final String ALPHA_KEY = "alpha";
	public static final String COLOR_KEY = "color";
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
}
