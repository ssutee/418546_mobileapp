package th.ac.ku.cs.sci.android.sutee.todo;

import th.ac.ku.cs.sci.android.sutee.todo.TaskDao.OnTaskChangeListener;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {

	TodoListAdapter adapter;
	TaskDao taskDao;
	ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		listView = (ListView) findViewById(R.id.listView);
		txtRemainingTasks = (TextView) findViewById(R.id.txtRemainingTasks);

		taskDao = new TaskDao(this);
		adapter = new TodoListAdapter(this, taskDao);
		listView.setAdapter(adapter);
		updateRemainingTasksText();

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int position, long id) {
				Task task = taskDao.getAt(position);
				task.setDone(!task.isDone());
				taskDao.update(task);
			}
		});

		taskDao.setOnTaskChangeListener(new OnTaskChangeListener() {
			@Override
			public void onTaskChange(TaskDao dao) {
				adapter.notifyDataSetChanged();
				updateRemainingTasksText();
			}
		});
	}

	private TextView txtRemainingTasks;

	private void updateRemainingTasksText() {
		txtRemainingTasks.setText(String.format("Remaining Tasks (%d/%d)",
				taskDao.size()-taskDao.getCountDoneTasks(), taskDao.size()));
	}

	public void addTask(View view) {
		EditText edtTaskName = (EditText) findViewById(R.id.edtTaskName);
		Task task = new Task(edtTaskName.getText().toString(), false);
		taskDao.insert(task);
	}

	@Override
	protected void onDestroy() {
		taskDao.close();
		super.onDestroy();
	}

}
