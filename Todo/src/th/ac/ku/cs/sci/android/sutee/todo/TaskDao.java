package th.ac.ku.cs.sci.android.sutee.todo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class TaskDao {

	private TodoOpenHelper openHelper;
	private SQLiteDatabase db;

	public interface OnTaskChangeListener {
		void onTaskChange(TaskDao dao);
	}

	private OnTaskChangeListener onTaskChangeListener;

	public void setOnTaskChangeListener(
			OnTaskChangeListener onTaskChangeListener) {
		this.onTaskChangeListener = onTaskChangeListener;
	}

	private void notifyTaskChange() {
		if (this.onTaskChangeListener != null) {
			this.onTaskChangeListener.onTaskChange(this);
		}
	}

	public TaskDao(Context context) {
		openHelper = new TodoOpenHelper(context);
		db = openHelper.getWritableDatabase();
	}

	public void insert(Task task) {
		db.insert(TaskTable.TABLE_NAME, null, task.toContentValues());
		notifyTaskChange();
	}

	public void update(Task task) {
		db.update(TaskTable.TABLE_NAME, task.toContentValues(), BaseColumns._ID
				+ " = " + task.getId(), null);
		notifyTaskChange();
	}

	private Task convertCursor(Cursor cursor) {
		long id = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));
		String name = cursor.getString(cursor
				.getColumnIndex(TaskTable.TaskColumns.NAME));
		boolean done = cursor.getInt(cursor
				.getColumnIndex(TaskTable.TaskColumns.DONE)) == 1;
		Date createdAt = new Date(cursor.getLong(cursor
				.getColumnIndex(TaskTable.TaskColumns.CREATED_AT)));
		Task task = new Task(name, done, createdAt);
		task.setId(id);
		return task;
	}

	public int size() {
		int result = 0;
		Cursor cursor = db.query(TaskTable.TABLE_NAME, null, null, null, null,
				null, null);
		result = cursor.getCount();
		cursor.close();
		return result;
	}

	public int getCountDoneTasks() {
		int result = 0;
		Cursor cursor = db.query(TaskTable.TABLE_NAME, null,
				TaskTable.TaskColumns.DONE + " = 1", null, null, null, null);
		result = cursor.getCount();
		cursor.close();
		return result;
	}

	public Task get(long id) {
		Cursor cursor = db.query(TaskTable.TABLE_NAME, null, BaseColumns._ID
				+ " = " + id, null, null, null, null);
		Task task = convertCursor(cursor);
		cursor.close();
		return task;
	}

	public Task getAt(int position) {
		Cursor cursor = db.query(TaskTable.TABLE_NAME, null, null, null, null,
				null, null);
		Task task = null;
		if (cursor.moveToPosition(position)) {
			task = convertCursor(cursor);
		}
		cursor.close();
		return task;
	}

	public List<Task> get() {
		ArrayList<Task> tasks = new ArrayList<Task>();
		Cursor cursor = db.query(TaskTable.TABLE_NAME, null, null, null, null,
				null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Task task = convertCursor(cursor);
			tasks.add(task);
			cursor.moveToNext();
		}
		cursor.close();
		return tasks;
	}

	public void delete(Task task) {
		db.delete(TaskTable.TABLE_NAME, BaseColumns._ID + " = " + task.getId(),
				null);
		notifyTaskChange();
	}

	public void close() {
		db.close();
	}
}
