package th.ac.ku.cs.sci.android.sutee.todo;

import java.util.Date;

import android.content.ContentValues;

public class Task {
	private long id;
	private String name;
	private boolean done;
	private Date createdAt;

	public Task(String name, boolean done, Date createdAt) {
		this.name = name;
		this.done = done;
		this.createdAt = createdAt;
	}

	public Task(String name, boolean done) {
		this(name, done, new Date());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(TaskTable.TaskColumns.NAME, this.name);
		values.put(TaskTable.TaskColumns.DONE, this.done);
		values.put(TaskTable.TaskColumns.CREATED_AT, this.createdAt.getTime());
		return values;
	}
}
