package th.ac.ku.cs.sci.android.sutee.todo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListAdapter;

public class TodoListAdapter extends BaseAdapter implements ListAdapter {

	private Context mContext;
	private TaskDao mTaskDao;

	public TodoListAdapter(Context context, TaskDao taskDao) {
		mContext = context;
		mTaskDao = taskDao;
	}

	@Override
	public int getCount() {
		return mTaskDao.size();
	}

	@Override
	public Object getItem(int position) {
		return mTaskDao.getAt(position);
	}

	@Override
	public long getItemId(int position) {
		Task task = mTaskDao.getAt(position);
		return task.getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row,
					parent, false);
		}

		Task task = (Task) getItem(position);

		CheckedTextView checkTextView = (CheckedTextView) convertView
				.findViewById(R.id.checkedTextView);
		checkTextView.setText(task.getName());
		checkTextView.setChecked(task.isDone());
		
		return convertView;
	}

}
