package th.ac.ku.cs.sci.android.sutee.todo;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class TaskTable {
	public static final String TABLE_NAME = "task";
	
	public static class TaskColumns implements BaseColumns {
		public static String NAME = "name";
		public static String DONE = "done";
		public static String CREATED_AT = "created_at"; 
	}
	
	static public void onCreate(SQLiteDatabase db) {
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE " + TABLE_NAME + " (");
		sb.append(BaseColumns._ID + " INTEGER PRIMARY KEY, ");
		sb.append(TaskColumns.NAME + " TEXT, ");
		sb.append(TaskColumns.DONE + " BOOLEAN, ");
		sb.append(TaskColumns.CREATED_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP");
		sb.append(");");
		db.execSQL(sb.toString());
	}
	
	static public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(db);
	}
}
