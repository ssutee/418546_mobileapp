package th.ac.ku.sci.cs.android.sutee.demolistview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class DotView extends View {

	private Paint paint;

	public interface OnDotViewPressListener {
		void onDotViewPress(DotView dotView, float x, float y);
	}

	public interface DotViewDataSource {
		int size(DotView dotView);
		Dot getDot(DotView dotView, int position);
	}

	private DotViewDataSource dataSource;
	private OnDotViewPressListener onDotViewPressListener;

	private void init() {
		this.paint = new Paint();
	}

	public DotView(Context context) {
		super(context);
		init();
	}

	public DotView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public DotView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public void setDataSource(DotViewDataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setOnDotViewPressListener(
			OnDotViewPressListener onDotViewPressListener) {
		this.onDotViewPressListener = onDotViewPressListener;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		paint.setColor(Color.BLUE);
		if (this.dataSource != null) {
			for (int position = 0; position < this.dataSource.size(this); ++position) {
				Dot dot = this.dataSource.getDot(this, position);
				canvas.drawCircle(dot.getCoordX(), dot.getCoordY(), 5, paint);
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getActionMasked()) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_MOVE:
			if (this.onDotViewPressListener != null) {
				this.onDotViewPressListener.onDotViewPress(this, event.getX(),
						event.getY());
				for (int index = 0; index < event.getHistorySize(); ++index) {
					this.onDotViewPressListener.onDotViewPress(this,
							event.getX(index), event.getY(index));
				}
			}
			return true;
		}
		return false;
	}
}
