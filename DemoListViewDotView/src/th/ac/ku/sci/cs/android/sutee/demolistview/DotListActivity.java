package th.ac.ku.sci.cs.android.sutee.demolistview;

import java.util.Random;

import th.ac.ku.sci.cs.android.sutee.demolistview.DotView.DotViewDataSource;
import th.ac.ku.sci.cs.android.sutee.demolistview.DotView.OnDotViewPressListener;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;

public class DotListActivity extends ListActivity implements
		Dots.OnDotsChangeListener {

	private Dots dots;
	private DotsAdapter adapter;
	private DotView dotView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		this.dotView = (DotView) findViewById(R.id.dotView);
		this.dots = new Dots();
		this.dots.setOnDotsChangeListener(this);
		this.adapter = new DotsAdapter(this, this.dots);
		setListAdapter(this.adapter);
		registerForContextMenu(getListView());

		this.dotView.setDataSource(new DotViewDataSource() {
			@Override
			public int size(DotView dotView) {
				return dots.size();
			}

			@Override
			public Dot getDot(DotView dotView, int position) {
				return dots.getDot(position);
			}
		});

		this.dotView.setOnDotViewPressListener(new OnDotViewPressListener() {
			@Override
			public void onDotViewPress(DotView dotView, float x, float y) {
				dots.addDot(new Dot((int) x, (int) y));
			}
		});

	}

	static final int EDIT_MENU = 0;
	static final int DELETE_MENU = 1;

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(Menu.NONE, EDIT_MENU, Menu.NONE, R.string.edit);
		menu.add(Menu.NONE, DELETE_MENU, Menu.NONE, R.string.delete);
	}

	private void deleteDot(int position) {
		new AlertDialog.Builder(this).setTitle("Confirm Delete")
				.setMessage("Are you sure?")
				.setPositiveButton("Yes", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// delete dot
					}
				}).setNegativeButton("No", null).create().show();
	}

	private void editDot(int position) {
		Dot dot = this.dots.getDot(position);
		View view = getLayoutInflater()
				.inflate(R.layout.edit_dialog_view, null);
		final EditText edtCoordX = (EditText) view.findViewById(R.id.edtCoordX);
		final EditText edtCoordY = (EditText) view.findViewById(R.id.edtCoordY);
		edtCoordX.setText(dot.getCoordX() + "");
		edtCoordY.setText(dot.getCoordY() + "");
		new AlertDialog.Builder(this).setTitle("Edit Dot").setView(view)
				.setPositiveButton("Yes", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// edit dot
					}
				}).setNegativeButton("No", null).create().show();
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		if (item.getItemId() == EDIT_MENU) {
			editDot(info.position);
		} else if (item.getItemId() == DELETE_MENU) {
			deleteDot(info.position);
		}
		return super.onContextItemSelected(item);
	}

	private Random random = new Random();

	public void randomDot(View view) {
		this.dots.addDot(new Dot(random.nextInt(this.dotView.getWidth()),
				random.nextInt(this.dotView.getHeight())));
	}

	@Override
	public void onDotsChange(Dots dots) {
		this.adapter.notifyDataSetChanged();
		this.dotView.invalidate();
	}
}
