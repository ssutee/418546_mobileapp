package th.ac.ku.sci.cs.android.sutee.demolistview;


public class Dot {
	private int coordX;
	private int coordY;
	private int color;
	private int size;
	private int id;

	public interface OnDotChangeListener {
		void dotChange(Dot dot);
	}

	private OnDotChangeListener onDotChangeListener;

	public void setOnDotChangeListener(OnDotChangeListener onDotChangeListener) {
		this.onDotChangeListener = onDotChangeListener;
	}

	public Dot(int coordX, int coordY) {
		super();
		this.coordX = coordX;
		this.coordY = coordY;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCoordX() {
		return coordX;
	}

	public void setCoordX(int coordX) {
		this.coordX = coordX;
		if (this.onDotChangeListener != null) {
			this.onDotChangeListener.dotChange(this);
		}
	}

	public int getCoordY() {
		return coordY;
	}

	public void setCoordY(int coordY) {
		this.coordY = coordY;
		if (this.onDotChangeListener != null) {
			this.onDotChangeListener.dotChange(this);
		}
	}
}
