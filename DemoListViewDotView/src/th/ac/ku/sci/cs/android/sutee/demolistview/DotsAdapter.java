package th.ac.ku.sci.cs.android.sutee.demolistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

public class DotsAdapter extends BaseAdapter implements ListAdapter {
	private Context context;
	private Dots dots;

	public DotsAdapter(Context context, Dots dots) {
		this.context = context;
		this.dots = dots;
	}

	@Override
	public int getCount() {
		return this.dots.size();
	}

	@Override
	public Object getItem(int position) {
		return this.dots.getDot(position);
	}

	@Override
	public long getItemId(int position) {
		return this.dots.getDot(position).getId();
	}

	public class ViewHolder {
		TextView txtCoordX;
		TextView txtCoordY;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;

		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.list_item, parent, false);
			viewHolder.txtCoordX = (TextView) convertView
					.findViewById(R.id.txtCoordX);
			viewHolder.txtCoordY = (TextView) convertView
					.findViewById(R.id.txtCoordY);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		Dot dot = (Dot) getItem(position);
		viewHolder.txtCoordX.setText(dot.getCoordX() + "");
		viewHolder.txtCoordY.setText(dot.getCoordY() + "");

		return convertView;
	}
}