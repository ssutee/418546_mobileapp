package th.ac.ku.sci.cs.android.sutee.demolistview;

interface IDotService {
	void randomDot(int rangeX, int rangeY);
}