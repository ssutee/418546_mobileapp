package th.ac.ku.sci.cs.android.sutee.demolistview;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class DotBootupReceiver extends BroadcastReceiver {

	private static final long FIVE_SECONDS = 5 * 1000;

	@Override
	public void onReceive(Context context, Intent intent) {
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		PendingIntent sender = PendingIntent.getBroadcast(context, 0,
				new Intent(context, AlarmReceiver.class),
				PendingIntent.FLAG_CANCEL_CURRENT);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.SECOND, 1);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				now.getTimeInMillis(), FIVE_SECONDS, sender);
	}
}
