package th.ac.ku.sci.cs.android.sutee.demolistview;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;

public class Dot extends ModelBase {
	@SuppressWarnings("unused")
	private Context context;
	private int id;
	private int coordX;
	private int coordY;

	public Dot() {
		super();
	}

	@Override
	public int getId() {
		return id;
	}

	public static Dot newInstance(Cursor cursor, Context context) {
		Dot dot = new Dot();
		dot.fromCursor(cursor, context);
		return dot;
	}	

	public Dot(int coordX, int coordY) {
		super();
		this.coordX = coordX;
		this.coordY = coordY;
	}

	@Override
	public void fromCursor(Cursor cursor, Context context) {
		this.id = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));
		this.coordX = cursor.getInt(cursor
				.getColumnIndex(DotTable.DotColumns.COORD_X));
		this.coordY = cursor.getInt(cursor
				.getColumnIndex(DotTable.DotColumns.COORD_Y));
		this.context = context;
	}

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(DotTable.DotColumns.COORD_X, this.coordX);
		values.put(DotTable.DotColumns.COORD_Y, this.coordY);
		return values;
	}

	public int getCoordX() {
		return coordX;
	}

	public void setCoordX(int coordX) {
		this.coordX = coordX;
	}

	public int getCoordY() {
		return coordY;
	}

	public void setCoordY(int coordY) {
		this.coordY = coordY;
	}

	public void setId(int id) {
		this.id = id;
	}

}