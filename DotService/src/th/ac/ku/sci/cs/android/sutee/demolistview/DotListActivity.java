package th.ac.ku.sci.cs.android.sutee.demolistview;

import th.ac.ku.sci.cs.android.sutee.demolistview.DotView.DotViewDataSource;
import th.ac.ku.sci.cs.android.sutee.demolistview.DotView.OnDotViewPressListener;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.ListView;

public class DotListActivity extends FragmentActivity implements
		LoaderCallbacks<Cursor> {

	private static final String TAG = "DotListActivity";
	private Dao<Dot> dotDao;
	private DotCursorAdapter adapter;
	private DotView dotView;
	private int dotCount = 0;
	private ListView listView;
	private IDotService dotService;
	private boolean bound = false;
	private ServiceConnection connection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "onServiceDisconnected");
			dotService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "onServiceConnected");
			dotService = IDotService.Stub.asInterface(service);
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		Log.d(TAG, "onStart");
		if (!bound) {
			bound = bindService(new Intent(this, DotService.class), connection,
					Context.BIND_AUTO_CREATE);
		}

		if (!bound) {
			Log.e(TAG, "Failed to bind to service");
			throw new RuntimeException("Failed to find to service");
		}

		listView.setAdapter(adapter);
	}

	@Override
	protected void onStop() {
		if (bound) {
			bound = false;
			unbindService(connection);
		}
		super.onStop();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			dotCount = savedInstanceState.getInt(DOT_COUNT_TAG);
		}

		setContentView(R.layout.main);

		listView = (ListView) findViewById(R.id.listView);
		dotView = (DotView) findViewById(R.id.dotView);

		adapter = new DotCursorAdapter(this, null);
		dotDao = new Dao<Dot>(Dot.class, this, DotProvider.DOT_CONTENT_URI);

		registerForContextMenu(listView);

		this.dotView.setDataSource(new DotViewDataSource() {
			@Override
			public int size(DotView dotView) {
				return dotDao.size();
			}

			@Override
			public Dot getDot(DotView dotView, int position) {
				return dotDao.get(position);
			}
		});

		this.dotView.setOnDotViewPressListener(new OnDotViewPressListener() {
			@Override
			public void onDotViewPress(DotView dotView, float x, float y) {
				dotDao.insert(new Dot((int) x, (int) y));
				dotCount += 1;
			}
		});
		getSupportLoaderManager().initLoader(0, null, this);
	}

	private static final String DOT_COUNT_TAG = "dotCount";

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(DOT_COUNT_TAG, this.dotCount);
	}

	static final int EDIT_MENU = 0;
	static final int DELETE_MENU = 1;
	static final int CLEAR_MENU = 2;

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(Menu.NONE, EDIT_MENU, Menu.NONE, R.string.edit);
		menu.add(Menu.NONE, DELETE_MENU, Menu.NONE, R.string.delete);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(Menu.NONE, CLEAR_MENU, Menu.NONE, R.string.clear);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case CLEAR_MENU:
			dotDao.destroy();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void deleteDot(int position) {
		new AlertDialog.Builder(this).setTitle("Confirm Delete")
				.setMessage("Are you sure?")
				.setPositiveButton("Yes", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// delete dot
					}
				}).setNegativeButton("No", null).create().show();
	}

	private void editDot(int position) {
		Dot dot = dotDao.get(position);
		View view = getLayoutInflater()
				.inflate(R.layout.edit_dialog_view, null);
		final EditText edtCoordX = (EditText) view.findViewById(R.id.edtCoordX);
		final EditText edtCoordY = (EditText) view.findViewById(R.id.edtCoordY);
		edtCoordX.setText(dot.getCoordX() + "");
		edtCoordY.setText(dot.getCoordY() + "");
		new AlertDialog.Builder(this).setTitle("Edit Dot").setView(view)
				.setPositiveButton("Yes", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// edit dot
					}
				}).setNegativeButton("No", null).create().show();
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		if (item.getItemId() == EDIT_MENU) {
			editDot(info.position);
		} else if (item.getItemId() == DELETE_MENU) {
			deleteDot(info.position);
		}
		return super.onContextItemSelected(item);
	}

	public void randomDot(View view) {
		try {
			if (bound) {
				this.dotCount += 1;
				dotService.randomDot(dotView.getWidth(), dotView.getHeight());
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle data) {
		return new CursorLoader(this, DotProvider.DOT_CONTENT_URI, null, null,
				null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		adapter.swapCursor(cursor);
		dotView.invalidate();
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		adapter.swapCursor(null);
	}
}
