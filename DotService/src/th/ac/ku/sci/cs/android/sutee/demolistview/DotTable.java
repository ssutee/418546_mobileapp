package th.ac.ku.sci.cs.android.sutee.demolistview;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;


public final class DotTable {
    public static final String TABLE_NAME = "dot";

    public static class DotColumns implements BaseColumns {
        public static final String COORD_X = "coord_x";
        public static final String COORD_Y = "coord_y";
    }



    public static void onCreate(SQLiteDatabase db) {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE " + DotTable.TABLE_NAME + " (");
        sb.append(BaseColumns._ID + " INTEGER PRIMARY KEY, ");
        sb.append(DotColumns.COORD_X + " INTEGER, ");
        sb.append(DotColumns.COORD_Y + " INTEGER");
        sb.append(");");
        db.execSQL(sb.toString());
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DotTable.TABLE_NAME);
        DotTable.onCreate(db);
    }


}