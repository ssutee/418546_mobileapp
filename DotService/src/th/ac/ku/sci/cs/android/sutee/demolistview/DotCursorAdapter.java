package th.ac.ku.sci.cs.android.sutee.demolistview;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DotCursorAdapter extends CursorAdapter {

	public DotCursorAdapter(Context context, Cursor c) {
		super(context, c, 0);
	}

	private static final class ViewHolder {
		TextView txtCoordX;
		TextView txtCoordY;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder viewHolder = (ViewHolder) view.getTag();
		Dot dot = Dot.newInstance(cursor, context);
		viewHolder.txtCoordX.setText(String.valueOf(dot.getCoordX()));
		viewHolder.txtCoordY.setText(String.valueOf(dot.getCoordY()));
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View view = LayoutInflater.from(context).inflate(R.layout.list_item,
				parent, false);
		ViewHolder viewHolder = new ViewHolder();
		viewHolder.txtCoordX = (TextView) view.findViewById(R.id.txtCoordX);
		viewHolder.txtCoordY = (TextView) view.findViewById(R.id.txtCoordY);
		view.setTag(viewHolder);
		return view;
	}

}
