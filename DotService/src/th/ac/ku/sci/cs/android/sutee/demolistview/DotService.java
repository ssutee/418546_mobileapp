package th.ac.ku.sci.cs.android.sutee.demolistview;

import java.util.Random;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;

public class DotService extends Service {

	private static final String TAG = "DotService";
	Dao<Dot> dotDao;
	private Random random = new Random();

	@Override
	public IBinder onBind(Intent intent) {
		return new IDotService.Stub() {
			@Override
			public void randomDot(int rangeX, int rangeY)
					throws RemoteException {
				int x = random.nextInt(rangeX);
				int y = random.nextInt(rangeY);
				alertNotificationDot(x, y);
				dotDao.insert(new Dot(x, y));
			}
		};
	}

	protected void alertNotificationDot(int x, int y) {		
		Log.d(TAG, "alertNotificationDot");
		if (x >= 100 && y >= 100) {
			long when = System.currentTimeMillis();
			NotificationManager notificationManager = (NotificationManager) this
					.getSystemService(Context.NOTIFICATION_SERVICE);

			String title = this.getString(R.string.app_name);
			Intent notificationIntent = new Intent(this, DotListActivity.class);
			// set intent so it does not start a new activity
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
			PendingIntent intent = PendingIntent.getActivity(this, 0,
					notificationIntent, 0);

			Notification notification = new NotificationCompat.Builder(this)
					.setSmallIcon(R.drawable.ic_launcher)
					.setContentTitle(title).setContentText("Found dot!!!")
					.setContentIntent(intent).setWhen(when).getNotification();

			notification.flags |= Notification.FLAG_AUTO_CANCEL
					| Notification.FLAG_SHOW_LIGHTS;
			notification.defaults |= Notification.DEFAULT_SOUND;

			notificationManager.notify(0, notification);
			Log.d(TAG, "notify notification");
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();
		dotDao = new Dao<Dot>(Dot.class, this, DotProvider.DOT_CONTENT_URI);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "onStartCommand");
		dotDao.insert(new Dot(random.nextInt(300), random.nextInt(300)));
		return Service.START_NOT_STICKY;
	}

}
