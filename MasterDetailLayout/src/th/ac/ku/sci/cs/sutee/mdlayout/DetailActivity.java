package th.ac.ku.sci.cs.sutee.mdlayout;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class DetailActivity extends FragmentActivity {

	public static final String NUMBER_TAG = "number";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			finish();
			return;
		}

		setContentView(R.layout.activity_detail);
		Intent intent = getIntent();
		int number = intent.getExtras().getInt(NUMBER_TAG);
		if (savedInstanceState == null) {
			getSupportFragmentManager()
					.beginTransaction()
					.add(R.id.detail, NumberFragment.newNumberFragment(number),
							MainActivity.NUMBER_FRAGMENT_TAG).commit();
		}
	}
}
