package th.ac.ku.sci.cs.sutee.mdlayout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NumberFragment extends Fragment {

	private static final String NUMBER_TAG = "number";

	private int mNumber = 0;

	public static Fragment newNumberFragment(int number) {
		Fragment fragment = new NumberFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(NUMBER_TAG, number);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState == null) {
			savedInstanceState = getArguments();
		}

		if (savedInstanceState != null) {
			mNumber = savedInstanceState.getInt(NUMBER_TAG);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt(NUMBER_TAG, mNumber);
		super.onSaveInstanceState(outState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_view, container, false);
		((TextView) view.findViewById(R.id.txtNumber)).setText(String
				.valueOf(mNumber));
		return view;
	}

	public int getNumber() {
		return mNumber;
	}

}
