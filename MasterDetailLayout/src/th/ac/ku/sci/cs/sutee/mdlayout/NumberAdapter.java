package th.ac.ku.sci.cs.sutee.mdlayout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

abstract public class NumberAdapter extends BaseAdapter implements ListAdapter {

	private Context mContext;

	public NumberAdapter(Context context) {
		mContext = context;
	}

	private static final class ViewHolder {
		TextView txtTitle;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.title_row, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.txtTitle = (TextView) convertView
					.findViewById(R.id.txtTitile);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.txtTitle.setText((String) getItem(position));

		return convertView;
	}

}
