package th.ac.ku.sci.cs.android.sutee.week2;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	}

	public void shareMessageUsingChooser(String mimeType, String subject,
			String message) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType(mimeType);
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_TEXT, message);
		try {
			startActivity(Intent.createChooser(intent, "Share message ..."));
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(
					this,
					"There are no chooser options installed for the "
							+ mimeType + " + type.", Toast.LENGTH_SHORT).show();
		}
	}

	public void openUrlInBrowser(String url) {
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = new MenuInflater(this);
		menuInflater.inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_share:
			Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
			shareMessageUsingChooser("text/*", "subject", "message");
			break;
		case R.id.menu_open_url:
			Toast.makeText(this, "Open URL", Toast.LENGTH_SHORT).show();
			openUrlInBrowser("http://www.ku.ac.th");
			break;
		default:
			return false;
		}
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == ANOTHER_ACTIVITY_REQUEST
				&& resultCode == Activity.RESULT_OK) {
			((TextView) findViewById(R.id.txtReturn)).setText(data
					.getStringExtra(AnotherActivity.SEND_BACK_MESSAGE_KEY));
		}
	}

	private static final int ANOTHER_ACTIVITY_REQUEST = 0;
	public static final String SEND_MESSAGE_KEY = "send message";

	public void sendText(View view) {
		Intent intent = new Intent(this, AnotherActivity.class);
		intent.putExtra(SEND_MESSAGE_KEY,
				((EditText) findViewById(R.id.editText1)).getText().toString());
		startActivityForResult(intent, ANOTHER_ACTIVITY_REQUEST);
	}

	public void shareText(View view) {
		MyApplication myApp = (MyApplication) getApplication();
		myApp.setMessage(((EditText) findViewById(R.id.editText2)).getText()
				.toString());
		startActivity(new Intent(this, AnotherActivity.class));
	}
}