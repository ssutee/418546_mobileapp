package th.ac.ku.sci.cs.android.sutee.week2;

import android.app.Application;

public class MyApplication extends Application {

	private String message;

	@Override
	public void onCreate() {
		super.onCreate();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
