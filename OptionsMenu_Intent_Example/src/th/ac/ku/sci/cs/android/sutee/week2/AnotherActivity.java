package th.ac.ku.sci.cs.android.sutee.week2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class AnotherActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.another);

		if (getIntent() != null
				&& getIntent().hasExtra(MainActivity.SEND_MESSAGE_KEY)) {
			((TextView) findViewById(R.id.txtReceive)).setText(getIntent()
					.getStringExtra(MainActivity.SEND_MESSAGE_KEY));
		}
		MyApplication myApp = (MyApplication)getApplication();
		((TextView)findViewById(R.id.txtShared)).setText(myApp.getMessage());
	}

	public static final String SEND_BACK_MESSAGE_KEY = "send back message";

	public void sendBackText(View view) {
		Intent data = new Intent();

		data.putExtra(SEND_BACK_MESSAGE_KEY,
				((EditText) findViewById(R.id.editText1)).getText().toString());
		setResult(Activity.RESULT_OK, data);
		finish();
	}
}
