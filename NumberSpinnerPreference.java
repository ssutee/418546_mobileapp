package th.ac.ku.android.hellopoly;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class NumberSpinnerPreference extends DialogPreference implements
		OnItemSelectedListener {

	private Context mContext;
	private Spinner mSpinner;
	private int mValue;

	public NumberSpinnerPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}

	public NumberSpinnerPreference(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
	}

	private void setValue(int value) {
		mValue = value;
		persistInt(value);
	}

	@Override
	protected void onDialogClosed(boolean positiveResult) {
		super.onDialogClosed(positiveResult);
		if (positiveResult) {
			setValue(mValue);
		}
	}

	@Override
	protected void onPrepareDialogBuilder(Builder builder) {
		super.onPrepareDialogBuilder(builder);
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.spinner_layout, null);
		mSpinner = (Spinner) view.findViewById(R.id.spinner1);
		mSpinner.setOnItemSelectedListener(this);
		ArrayAdapter adapter = ArrayAdapter.createFromResource(mContext,
				R.array.numbers, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinner.setAdapter(adapter);
		mSpinner.setSelection(getPersistedInt(mValue));
		builder.setView(view);
	}

	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		return a.getInt(index, mValue);
	}

	@Override
	protected void onSetInitialValue(boolean restorePersistedValue,
			Object defaultValue) {
		setValue(restorePersistedValue ? getPersistedInt(mValue)
				: ((Integer) defaultValue).intValue());
	}

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view,
			int position, long id) {
		setValue(Integer.parseInt(mSpinner.getItemAtPosition(position)
				.toString()));
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

}
