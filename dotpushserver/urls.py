from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dotpush.views.home', name='home'),
    # url(r'^dotpush/', include('dotpush.foo.urls')),

    url(r'^dots/', include('dots.urls')),
    url(r'^devices/', include('devices.urls')),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
