from django.contrib import admin
from devices.models import Device

class DeviceAdmin(admin.ModelAdmin):
    list_display = ('registration_id', 'last_notified_at', )

admin.site.register(Device, DeviceAdmin)
