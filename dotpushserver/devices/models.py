from django.db import models
import datetime

class Device(models.Model):
    registration_id = models.CharField(blank=False, max_length=1024)
    last_notified_at = models.DateTimeField(blank=True, default=datetime.datetime.now)
    
    def __unicode__(self):
        return u"Device %s" % self.registration_id


