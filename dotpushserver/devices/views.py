#-*- coding: utf-8 -*-

from django.views.decorators.csrf import csrf_exempt
from django.utils import simplejson
from django.http import HttpResponse, HttpResponseRedirect
from devices.models import Device
from django_json import dumps

@csrf_exempt
def index(request):
    if len(request.POST) == 0:
        return HttpResponse(dumps(Device.objects.all()), 
            mimetype="application/json")
    else:
        registration_id = request.POST.get('reg_id')
        if registration_id:
            device, created = Device.objects.get_or_create(registration_id=registration_id)
    return HttpResponse('{"pk": %d}' % (device.id), mimetype="application/json")
