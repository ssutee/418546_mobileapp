from django.contrib import admin
from dots.models import Dot

class DotAdmin(admin.ModelAdmin):
    list_display = ('x', 'y', 'created_at', 'updated_at', )

admin.site.register(Dot, DotAdmin)