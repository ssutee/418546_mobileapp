#-*- coding: utf-8 -*-

from django.views.decorators.csrf import csrf_exempt
from django.utils import simplejson
from django.http import HttpResponse, HttpResponseRedirect
from dots.models import Dot
from django_json import dumps

@csrf_exempt
def index(request):
    if len(request.POST) == 0:
        return HttpResponse(dumps(Dot.objects.all()), 
            mimetype="application/json")
    else:
        x = request.POST.get('x')
        y = request.POST.get('y')
        if x and y:
            dot = Dot.objects.create(x=x, y=y)
            dot.save()
            return HttpResponse('{"pk": %d}' % (dot.id), mimetype="application/json")
    return HttpResponse('{"result":"fail"}', mimetype="application/json")
