from django.db import models
from datetime import datetime
from django.db.models.signals import post_save
from gcm.gcm import GCM
from devices.models import Device
from django.conf import settings

class Dot(models.Model):
    x = models.IntegerField()
    y = models.IntegerField()
    updated_at = models.DateTimeField()
    created_at = models.DateTimeField()
    
    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = datetime.today()
        self.updated_at = datetime.today()        
        super(Dot, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return u'Dot %d : %d' % (self.x, self.y)

def send_push_notifications(sender, instance, signal, *args, **kwargs):
    gcm = GCM(settings.GCM_API_KEY)    
    data = {}    
    reg_ids = []
    
    for device in Device.objects.all():
        reg_ids.append(device.registration_id)

    response = gcm.json_request(
        registration_ids=reg_ids, 
        data=data, collapse_key='dotpushrefresh', 
        delay_while_idle=True, time_to_live=3600)
        
    print response
                
post_save.connect(send_push_notifications, sender=Dot)
