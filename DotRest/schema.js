{
	"package" : "th.ac.ku.sci.cs.sutee.dotrest.model",
	"prefix" : "Dot",
	"database" : "dot.db",
	"tables" : ["dot"],
	"dot" : {
		"columns" : 
			[
			 {
				 "name" : "x",
				 "type" : "integer"
			 },
			 {
				 "name" : "y",
				 "type" : "integer"
			 },
			 {
				 "name" : "color",
				 "type" : "integer"
			 },
			 {
				 "name" : "gid",
				 "type" : "varchar(40)",
				 "options" : "unique"
			 },
			 {
				 "name" : "updated_at",
				 "type" : "timestamp",
				 "options" : "default current_timestamp"
			 },
			 {
				 "name" : "created_at",
				 "type" : "timestamp",
				 "options" : "default current_timestamp"
			 },
			 {
				 "name" : "state",
				 "type" : "integer"
			 }
			]
	}
}