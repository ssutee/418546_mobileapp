package th.ac.ku.sci.cs.sutee.dotrest.service;

import th.ac.ku.sci.cs.sutee.dotrest.model.Dot;

interface IDotRestService {
	void deleteDot(in Dot dot);
	void updateDot(in Dot dot);
	void insertDot(in Dot dot);
	void fetchDots();
}