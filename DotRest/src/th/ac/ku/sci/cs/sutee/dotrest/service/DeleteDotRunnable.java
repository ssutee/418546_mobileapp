package th.ac.ku.sci.cs.sutee.dotrest.service;

import th.ac.ku.sci.cs.sutee.dotrest.model.Dao;
import th.ac.ku.sci.cs.sutee.dotrest.model.Dot;
import th.ac.ku.sci.cs.sutee.dotrest.model.DotProvider;
import android.content.Context;
import android.os.Handler;

import com.touchsi.sutee.android.rest.RestClient;
import com.touchsi.sutee.android.rest.RestClient.OnRequestFinishListener;
import com.touchsi.sutee.android.rest.RestClient.RequestMethod;

public class DeleteDotRunnable implements Runnable, OnRequestFinishListener {

	private Handler mHandler;
	private Dot mDot;
	private Dao<Dot> dotDao;

	public DeleteDotRunnable(Context context, Handler handler, Dot dot) {
		mHandler = handler;
		mDot = dot;
		dotDao = new Dao<Dot>(Dot.class, context, DotProvider.DOT_CONTENT_URI);
	}

	@Override
	public void run() {
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		RestClient restClient = new RestClient(DotRestUtilities.DOTS_URL + "/"
				+ mDot.getRid() + "/", mHandler);
		restClient.setOnRequestFinishListener(this);
		restClient.addParam("_method", "DELETE");
		restClient.execute(RequestMethod.POST);
	}

	@Override
	public void onRequestFinish(RequestMethod method, int responseCode,
			String message, String response) {
		dotDao.delete(mDot);
	}

	@Override
	public void onRequestFinishWithError(Exception e) {
	}
}
