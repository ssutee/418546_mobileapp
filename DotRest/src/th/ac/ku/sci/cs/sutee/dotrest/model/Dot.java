package th.ac.ku.sci.cs.sutee.dotrest.model;

import java.text.ParseException;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import th.ac.ku.sci.cs.sutee.dotrest.service.DotRestUtilities;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

public class Dot extends ModelBase implements Parcelable {

	public enum State {
		NORMAL, DELETE, UPDATE, INSERT
	}

	@SuppressWarnings("unused")
	private static final String TAG = "Dot";

	private int id;
	private int x;
	private int y;
	private int color;
	private int rid;
	private Date updatedAt;
	private Date createdAt;
	private int state;

	public Dot() {
		super();
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void fromCursor(Cursor cursor, Context context) {
		this.id = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));
		this.x = cursor.getInt(cursor.getColumnIndex(DotTable.DotColumns.X));
		this.y = cursor.getInt(cursor.getColumnIndex(DotTable.DotColumns.Y));
		this.color = cursor.getInt(cursor
				.getColumnIndex(DotTable.DotColumns.COLOR));
		this.updatedAt = new Date(cursor.getLong(cursor
				.getColumnIndex(DotTable.DotColumns.UPDATED_AT)));
		this.createdAt = new Date(cursor.getLong(cursor
				.getColumnIndex(DotTable.DotColumns.CREATED_AT)));
		this.state = cursor.getInt(cursor
				.getColumnIndex(DotTable.DotColumns.STATE));
		this.rid = cursor
				.getInt(cursor.getColumnIndex(DotTable.DotColumns.RID));
	}

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(DotTable.DotColumns.X, this.x);
		values.put(DotTable.DotColumns.Y, this.y);
		values.put(DotTable.DotColumns.COLOR, this.color);
		values.put(DotTable.DotColumns.UPDATED_AT, this.updatedAt.getTime());
		values.put(DotTable.DotColumns.CREATED_AT, this.createdAt.getTime());
		values.put(DotTable.DotColumns.STATE, this.state);
		values.put(DotTable.DotColumns.RID, this.rid);
		return values;
	}

	public static Dot newInstance(Cursor cursor, Context context) {
		Dot dot = new Dot();
		dot.fromCursor(cursor, context);
		return dot;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public State getState() {
		return State.values()[state];
	}

	public void setState(State state) {
		this.state = state.ordinal();
	}

	public void setId(int id) {
		this.id = id;
	}

	public static final Parcelable.Creator<Dot> CREATOR = new Creator<Dot>() {

		@Override
		public Dot[] newArray(int size) {
			return new Dot[size];
		}

		@Override
		public Dot createFromParcel(Parcel source) {
			return new Dot(source);
		}
	};

	private Dot(Parcel parcel) {
		readFromParcel(parcel);
	}

	public void readFromParcel(Parcel parcel) {
		id = parcel.readInt();
		x = parcel.readInt();
		y = parcel.readInt();
		color = parcel.readInt();
		rid = parcel.readInt();
		updatedAt = new Date(parcel.readLong());
		createdAt = new Date(parcel.readLong());
		state = parcel.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeInt(id);
		parcel.writeInt(x);
		parcel.writeInt(y);
		parcel.writeInt(color);
		parcel.writeInt(rid);
		parcel.writeLong(updatedAt.getTime());
		parcel.writeLong(createdAt.getTime());
		parcel.writeInt(state);
	}

	public void readFromJSON(JSONObject jsonObject) throws JSONException,
			ParseException {
		x = jsonObject.getInt(DotRestUtilities.X_KEY);
		y = jsonObject.getInt(DotRestUtilities.Y_KEY);
		rid = jsonObject.getInt(DotRestUtilities.RID_KEY);
		color = jsonObject.getInt(DotRestUtilities.COLOR_KEY);
		updatedAt = DotRestUtilities.dateFormat.parse(jsonObject
				.getString(DotRestUtilities.UPDATED_AT_KEY));
	}
}