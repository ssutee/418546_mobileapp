package th.ac.ku.sci.cs.sutee.dotrest.service;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import th.ac.ku.sci.cs.sutee.dotrest.model.Dao;
import th.ac.ku.sci.cs.sutee.dotrest.model.Dot;
import th.ac.ku.sci.cs.sutee.dotrest.model.Dot.State;
import th.ac.ku.sci.cs.sutee.dotrest.model.DotProvider;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;

public class DotRestService extends Service {

	@SuppressWarnings("unused")
	private static final String TAG = "DotRestService";
	private Dao<Dot> dotDao;
	private ThreadPoolExecutor executor;
	private Handler mHandler = new Handler();

	@Override
	public void onCreate() {
		super.onCreate();
		dotDao = new Dao<Dot>(Dot.class, this, DotProvider.DOT_CONTENT_URI);
		executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return new IDotRestService.Stub() {

			@Override
			public void updateDot(Dot dot) throws RemoteException {
				dot.setState(State.UPDATE);
				dot.setUpdatedAt(new Date());
				dotDao.update(dot);
				updateDotServerSide(dot);
			}

			@Override
			public void fetchDots() throws RemoteException {
				fetchDotsServerSide();
			}

			@Override
			public void deleteDot(Dot dot) throws RemoteException {
				dot.setState(State.DELETE);
				dot.setUpdatedAt(new Date());
				dotDao.update(dot);
				deleteDotServerSide(dot);
			}

			@Override
			public void insertDot(Dot dot) throws RemoteException {
				dot.setState(State.INSERT);
				dot.setUpdatedAt(new Date());
				dot.setCreatedAt(new Date());
				dotDao.insert(dot);
				insertDotServerSide(dot);
			}
		};
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		for (Dot dot : dotDao.get(null, null)) {
			switch (dot.getState()) {
			case DELETE:
				deleteDotServerSide(dot);
				break;
			case INSERT:
				insertDotServerSide(dot);
				break;
			case UPDATE:
				updateDotServerSide(dot);
				break;
			}
		}
		return Service.START_NOT_STICKY;
	}

	protected void fetchDotsServerSide() {
		executor.execute(new FetchDotsRunnable(this, mHandler));
	}

	protected void updateDotServerSide(Dot dot) {

	}

	protected void deleteDotServerSide(Dot dot) {
		executor.execute(new DeleteDotRunnable(this, mHandler, dot));
	}

	protected void insertDotServerSide(Dot dot) {

	}

}
