package th.ac.ku.sci.cs.sutee.dotrest;

import th.ac.ku.sci.cs.sutee.dotrest.model.Dao;
import th.ac.ku.sci.cs.sutee.dotrest.model.Dot;
import th.ac.ku.sci.cs.sutee.dotrest.model.Dot.State;
import th.ac.ku.sci.cs.sutee.dotrest.model.DotProvider;
import th.ac.ku.sci.cs.sutee.dotrest.service.DotRestService;
import th.ac.ku.sci.cs.sutee.dotrest.service.IDotRestService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

public class MainActivity extends FragmentActivity implements
		LoaderCallbacks<Cursor>, OnItemClickListener {

	private static final int MENU_ITEM_EDIT = 1001;
	private static final int MENU_ITEM_DELETE = 1002;
	private static final String TAG = "MainActivity";
	private IDotRestService dotRestService;
	private ServiceConnection connection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			dotRestService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			dotRestService = IDotRestService.Stub.asInterface(service);
			try {
				dotRestService.fetchDots();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	};

	private Button btnRefresh;
	private ProgressBar spinner;
	private ListView listView;
	private DotCursorAdapter adapter;
	private Dao<Dot> dotDao;

	@Override
	protected void onStart() {
		super.onStart();
		bindService(new Intent(this, DotRestService.class), connection,
				Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		unbindService(connection);
		super.onStop();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		dotDao = new Dao<Dot>(Dot.class, this, DotProvider.DOT_CONTENT_URI);

		setContentView(R.layout.activity_main);
		btnRefresh = (Button) findViewById(R.id.btnRefresh);
		spinner = (ProgressBar) findViewById(R.id.spinner);
		listView = (ListView) findViewById(R.id.listView);

		spinner.setVisibility(View.INVISIBLE);
		adapter = new DotCursorAdapter(this, null);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);

		registerForContextMenu(listView);

		getSupportLoaderManager().initLoader(0, null, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		switch (item.getItemId()) {
		case MENU_ITEM_DELETE:
			deleteDot(info.position);
			return true;
		case MENU_ITEM_EDIT:
			editDot(info.position);
			return true;
		}

		return super.onContextItemSelected(item);
	}

	private void editDot(int position) {

	}

	private void deleteDot(int position) {
		Log.d(TAG, "deleteDot");
		try {
			dotRestService.deleteDot(dotDao.get(position));
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		Dot dot = dotDao.get(info.position);
		if (dot.getState() != State.DELETE) {
			menu.addSubMenu(Menu.NONE, MENU_ITEM_EDIT, Menu.NONE, R.string.edit);
			menu.addSubMenu(Menu.NONE, MENU_ITEM_DELETE, Menu.NONE,
					R.string.delete);
		}
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle data) {
		return new CursorLoader(this, DotProvider.DOT_CONTENT_URI, null, null,
				null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		btnRefresh.setVisibility(View.VISIBLE);
		spinner.setVisibility(View.INVISIBLE);
		adapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		adapter.swapCursor(null);
	}

	public void refresh(View view) {
		btnRefresh.setVisibility(View.INVISIBLE);
		spinner.setVisibility(View.VISIBLE);
		try {
			dotRestService.fetchDots();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long id) {
		Dot dot = dotDao.get(position);
		Log.d(TAG, dot.getRid() + "");
	}
}
