package th.ac.ku.sci.cs.sutee.dotrest.model;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;


public final class DotTable {
    public static final String TABLE_NAME = "dot";

    public static class DotColumns implements BaseColumns {
        public static final String X = "x";
        public static final String Y = "y";
        public static final String COLOR = "color";
        public static final String UPDATED_AT = "updated_at";
        public static final String CREATED_AT = "created_at";
        public static final String STATE = "state";
        public static final String RID = "rid";
    }



    public static void onCreate(SQLiteDatabase db) {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE " + DotTable.TABLE_NAME + " (");
        sb.append(BaseColumns._ID + " INTEGER PRIMARY KEY, ");
        sb.append(DotColumns.X + " INTEGER, ");
        sb.append(DotColumns.Y + " INTEGER, ");
        sb.append(DotColumns.COLOR + " INTEGER, ");
        sb.append(DotColumns.UPDATED_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ");
        sb.append(DotColumns.CREATED_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ");
        sb.append(DotColumns.RID + " INTEGER, ");
        sb.append(DotColumns.STATE + " INTEGER");
        sb.append(");");
        db.execSQL(sb.toString());
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DotTable.TABLE_NAME);
        DotTable.onCreate(db);
    }


}