package th.ac.ku.sci.cs.sutee.dotrest.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import th.ac.ku.sci.cs.sutee.dotrest.model.Dao;
import th.ac.ku.sci.cs.sutee.dotrest.model.Dot;
import th.ac.ku.sci.cs.sutee.dotrest.model.DotProvider;
import th.ac.ku.sci.cs.sutee.dotrest.model.DotTable;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.touchsi.sutee.android.rest.RestClient;
import com.touchsi.sutee.android.rest.RestClient.OnRequestFinishListener;
import com.touchsi.sutee.android.rest.RestClient.RequestMethod;

public final class FetchDotsRunnable implements Runnable,
		OnRequestFinishListener {

	private static final String TAG = "FetchDotsRunnable";

	private Context mContext;
	private Handler mHandler;
	private Dao<Dot> dotDao;

	public FetchDotsRunnable(Context context, Handler handler) {
		this.mContext = context;
		this.mHandler = handler;
		this.dotDao = new Dao<Dot>(Dot.class, context,
				DotProvider.DOT_CONTENT_URI);
	}

	@Override
	public void run() {
		RestClient restClient = new RestClient(DotRestUtilities.DOTS_URL,
				mHandler);
		restClient.setOnRequestFinishListener(this);
		restClient.execute(RequestMethod.GET);
	}

	@Override
	public void onRequestFinish(RequestMethod method, int responseCode,
			String message, String response) {
		if (responseCode == 200) {
			handleJSONDots(response);
		} else {
			mContext.getContentResolver().notifyChange(
					DotProvider.DOT_CONTENT_URI, null);
		}
	}

	private void handleJSONDots(String response) {
		mContext.getContentResolver().notifyChange(DotProvider.DOT_CONTENT_URI,
				null);
		try {
			JSONArray jsonArray = new JSONArray(response);
			ArrayList<Integer> gids = new ArrayList<Integer>();
			for (int position = 0; position < jsonArray.length(); ++position) {
				JSONObject jsonObject = jsonArray.getJSONObject(position);
				Date updatedAt = DotRestUtilities.dateFormat.parse(jsonObject
						.getString(DotRestUtilities.UPDATED_AT_KEY));
				Integer rid = jsonObject.getInt(DotRestUtilities.RID_KEY);
				List<Dot> dots = dotDao.get(DotTable.DotColumns.RID + " = "
						+ rid, null);
				if (dots.size() == 0) {
					Dot dot = new Dot();
					dot.setCreatedAt(new Date());
					dot.readFromJSON(jsonObject);
					dotDao.insert(dot);
					Log.d(TAG, "insert");
				} else if (dots.get(0).getUpdatedAt().before(updatedAt)) {
					Dot dot = dots.get(0);
					dot.readFromJSON(jsonObject);
					dotDao.update(dot);
					Log.d(TAG, "update");
				} else {
					Log.d(TAG, "idle");
				}
				gids.add(rid);
			}

			for (Dot dot : dotDao.get(null, null)) {
				if (!gids.contains(dot.getRid())) {
					dotDao.delete(dot);
					Log.d(TAG, "delete");
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onRequestFinishWithError(Exception e) {
		mContext.getContentResolver().notifyChange(DotProvider.DOT_CONTENT_URI,
				null);
	}
}