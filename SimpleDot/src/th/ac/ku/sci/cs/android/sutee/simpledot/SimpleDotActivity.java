package th.ac.ku.sci.cs.android.sutee.simpledot;

import java.util.Random;

import th.ac.ku.sci.cs.android.sutee.simpledot.Dot.DotChangeListener;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class SimpleDotActivity extends Activity implements DotChangeListener {
	private static final String TAG = "SimpleDotActivity";

	Dot dotModel = new Dot(0, 0);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		dotModel.setDotChangeListener(this);
		dotModel.setCoordX(0);
		dotModel.setCoordY(0);
	}

	public void randomDot(View view) {
		Log.d(TAG, "generate a dot");
		Random random = new Random();
		dotModel.setCoordX(random.nextInt(200));
		dotModel.setCoordY(random.nextInt(200));
	}

	public void dotChange(Dot dot) {
		TextView coordX = (TextView) findViewById(R.id.txtCoordX);
		TextView coordY = (TextView) findViewById(R.id.txtCoordY);
		coordX.setText(String.valueOf(dot.getCoordX()));
		coordY.setText(String.valueOf(dot.getCoordY()));
	}
}