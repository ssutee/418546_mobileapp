package th.ac.ku.sci.cs.android.sutee.simpledot;

public class Dot {
	private int coordX;
	private int coordY;

	public interface DotChangeListener {
		void dotChange(Dot dot);
	}

	private DotChangeListener dotChangeListener;

	public void setDotChangeListener(DotChangeListener dotChangeListener) {
		this.dotChangeListener = dotChangeListener;
	}

	public Dot(int coordX, int coordY) {
		super();
		this.coordX = coordX;
		this.coordY = coordY;
	}

	public int getCoordX() {
		return coordX;
	}

	public void setCoordX(int coordX) {
		this.coordX = coordX;
		if (this.dotChangeListener != null) {
			this.dotChangeListener.dotChange(this);
		}
	}

	public int getCoordY() {
		return coordY;
	}

	public void setCoordY(int coordY) {
		this.coordY = coordY;
		if (this.dotChangeListener != null) {
			this.dotChangeListener.dotChange(this);
		}
	}
}
